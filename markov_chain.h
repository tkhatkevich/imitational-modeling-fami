#pragma once

#include <vector>
#include <random>

class TMarkovChain {
public:
    typedef std::vector<double> TVector;
    typedef std::vector<TVector> TMatrix; 
    typedef std::random_device TRandomGenerator;

private:
    TVector QVector;
    TMatrix QMatrix;
    TRandomGenerator RandomGenerator;

public:
    TMarkovChain (const TVector& initialProbs,
                  const TMatrix& transitionMatrix
                  ) 
    {
        size_t nStates = initialProbs.size();
        QVector.resize(nStates, initialProbs[0]);
        QMatrix.resize(nStates);

        for (size_t i = 1; i < nStates; ++i) {
            QVector[i] = QVector[i - 1] + initialProbs[i]; 
        }
        
      	for (size_t i = 0; i < nStates; ++i) {
		    QMatrix[i].resize(nStates, transitionMatrix[i][0]);
		    for (size_t j = 1; j < nStates; ++j) {
			    QMatrix[i][j] = QMatrix[i][j-1] + transitionMatrix[i][j];
		    }
	    }
    }

    size_t GetInitialState();

    size_t GetNextState(size_t prevState);

    double GetBaseRandom();
};

void EstimateParameters(const std::vector<size_t>& experiment,
                        size_t nStates,
                        TMarkovChain::TVector* stateProbsEstimation, 
                        TMarkovChain::TMatrix* trMatrixEstimation);

#include <iostream>
#include <fstream>

#include "markov_chain.h"

typedef std::vector<double> TVector;
typedef std::vector<TVector> TMatrix; 


void GetDataForMarkovChain(TVector* initialProbs, TMatrix* transitionMatrix) {
	std::ifstream fin("input.txt");
	size_t nStates;

	fin >> nStates;
	initialProbs->resize(nStates);
	transitionMatrix->resize(nStates);

	for (size_t i = 0; i < nStates; ++i) {
		fin >> initialProbs->at(i);
	}

	for (int i = 0; i < nStates; ++i) {
		transitionMatrix->at(i).resize(nStates);
		for (int j = 0; j < nStates; ++j) {
			fin >> transitionMatrix->at(i).at(j);
		}
	}

	fin.close();
}

void OutputMarkovChainResults(const TVector& stateProbs, const TMatrix& transitionMatrix) {
    std::ofstream fout("output.txt");
    
    for (size_t i = 0; i < stateProbs.size(); ++i) {
        fout << stateProbs[i] << " ";
    }
    fout << std::endl << std::endl;
    
    for (size_t i = 0; i < transitionMatrix.size(); ++i) {
        for (size_t j = 0; j < transitionMatrix[i].size(); ++j) {
            fout << transitionMatrix[i][j] << " ";
        }
        fout << std::endl;
    }
    
    fout.close();
}

void GetDataForSMO(TVector* initialProbs, TMatrix* transitionMatrix, size_t* nWorkers) {
    std::ifstream fin("input.txt");
    size_t nDevices;
    double pCrash;
    double pRepair;
    fin >> nDevices >> *nWorkers >> pCrash >> pRepair;
    fin.close();

    initialProbs->resize(nDevices + 1, 0);
    initialProbs->at(0) = 1;

    transitionMatrix->resize(nDevices + 1);
    for (size_t i = 1; i < nDevices; ++i) {
        TVector& curRaw = transitionMatrix->at(i);
        curRaw.resize(nDevices + 1);
        
        curRaw[i - 1] = std::min(i, *nWorkers) * pRepair;
        curRaw[i + 1] = (nDevices - i) * pCrash;
        curRaw[i] = 1 - curRaw[i - 1] - curRaw[i + 1];
    }

    {
        TVector& curRaw = transitionMatrix->at(0);
        curRaw.resize(nDevices + 1);
        curRaw[1] = nDevices * pCrash;
        curRaw[0] = 1 - curRaw[1];
    }

    {
        TVector& curRaw = transitionMatrix->at(nDevices);
        curRaw.resize(nDevices + 1);
        curRaw[nDevices - 1] = *nWorkers * pRepair;
        curRaw[nDevices] = 1 - curRaw[nDevices - 1];
    }
}

int main() {
    TVector initProbabilities;
    TMatrix transitionMatrix;
    size_t nWorkers;
    GetDataForSMO(&initProbabilities, &transitionMatrix, &nWorkers);
    TMarkovChain markovChain(initProbabilities, transitionMatrix);

    size_t sampleSize;
    std::cout << "Enter size of a sample:" << std::endl;
    std::cin >> sampleSize;

    std::vector<size_t> experiment(sampleSize, markovChain.GetInitialState());
    for (size_t i = 1; i < sampleSize; ++i) {
        experiment[i] = markovChain.GetNextState(experiment[i - 1]);
    }

    EstimateParameters(experiment, initProbabilities.size(), &initProbabilities, &transitionMatrix);
    OutputMarkovChainResults(initProbabilities, transitionMatrix);

    double avgBroken = 0;
    double avgFreeWorkers = 0;
    for (size_t i = 0; i < initProbabilities.size(); ++i) {
        avgBroken += initProbabilities[i] * i;
        avgFreeWorkers += initProbabilities[i] * std::max<int>(0, nWorkers - i);
    }

    std::cout << "average broken devices:\t" << avgBroken << std::endl;
    std::cout << "average free workers:\t"  << avgFreeWorkers << std::endl;

	return 0;
}
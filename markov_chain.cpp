#include "markov_chain.h"
#include <algorithm>

void InitializeMatrices(TMarkovChain::TVector* stateProbsEstimation,
                        TMarkovChain::TMatrix* trMatrixEstimation,
                        size_t nStates)
{
    stateProbsEstimation->clear();
    trMatrixEstimation->clear();
    stateProbsEstimation->resize(nStates, 0);
    trMatrixEstimation->resize(nStates);
    
    for (size_t i = 0; i < nStates; ++i) {
        trMatrixEstimation->at(i).resize(nStates, 0);
    }
}

void EstimateParameters(const std::vector<size_t>& experiment,
                        size_t nStates,
                        TMarkovChain::TVector* stateProbsEstimation, 
                        TMarkovChain::TMatrix* trMatrixEstimation) 
{
    InitializeMatrices(stateProbsEstimation, trMatrixEstimation, nStates);

    for (size_t i = 0; i + 1 < experiment.size(); ++i) {
        stateProbsEstimation->at(experiment[i]) += 1;
        trMatrixEstimation->at(experiment[i])[experiment[i + 1]] += 1;
    }

    //normalization
    for (size_t i = 0; i < trMatrixEstimation->size(); ++i) {
        for (size_t j = 0; j < trMatrixEstimation->at(i).size(); ++j) {
            double frequence = stateProbsEstimation->at(i);
      
            if (frequence != 0) {
                trMatrixEstimation->at(i)[j] /= frequence;
            } else {
                trMatrixEstimation->at(i)[j] = 0;
            }
        }
    }
    
    stateProbsEstimation->at(experiment[experiment.size() - 1]) += 1;
    for (size_t i = 0; i < stateProbsEstimation->size(); ++i) {
        stateProbsEstimation->at(i) /= experiment.size();
    }
}

double TMarkovChain::GetBaseRandom() {
    return RandomGenerator() / ((double) RandomGenerator.max() + 1);
}

size_t TMarkovChain::GetNextState(size_t prevState) {
    double baseRandom = GetBaseRandom();
    TVector::const_iterator it = std::lower_bound(QMatrix[prevState].begin(), QMatrix[prevState].end(), baseRandom);
    return it - QMatrix[prevState].begin();
}

size_t TMarkovChain::GetInitialState() {
    double baseRandom = GetBaseRandom();
    TVector::const_iterator it = std::lower_bound(QVector.begin(), QVector.end(), baseRandom);
    return it - QVector.begin();
}